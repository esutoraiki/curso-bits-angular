import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'tarea1';
  products = [
    {
      name: 'Teléfono XL',
      price: 799,
      cant: 10,
      description: 'Un teléfono grande con una de las mejores pantallas'
    },
    {
      name: 'Teléfono Mini',
      price: 699,
      cant: 3,
      description: 'Un gran teléfono con una de las mejores cámaras'
    },
    {
      name: 'Teléfono Estándar',
      price: 299,
      cant: 3,
      description: ''
    },
  ];

  decrement = (prod) => {
    if (prod.cant > 0) {
      prod.cant--;
      return prod.cant;
    }
  }
}
