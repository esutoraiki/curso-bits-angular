import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'quantity'
})
export class QuantityPipe implements PipeTransform {

  transform(value: string, cant: number): string {
    return value + ' -- cantidad (' + cant + ')';
  }

}
